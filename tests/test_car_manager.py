from src.cars import CarManager


def test_car_manager_should_read_data_from_csv_file():
    test_car_manager = CarManager()
    test_car_manager.load_from_csv('data_files/test_cars.csv')


def test_car_manager_getting_cars_list():
    test_car_manager = CarManager()
    test_car_manager.load_from_csv('data_files/test_cars.csv')
    assert len(test_car_manager.get_cars()) == 3
